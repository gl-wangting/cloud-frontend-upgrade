
export function isOverseasArea() {
  var hostName = window.location.hostname
  if (hostName === 'www.goodcloud.xyz' ||
    hostName === 'jp.goodcloud.xyz' ||
    hostName === 'us.goodcloud.xyz' ||
    hostName === 'eu.goodcloud.xyz' ||
    hostName === 'dev.goodcloud.xyz' ||
    hostName === 'test.goodcloud.xyz') {
    return true
  } else if (hostName === 'cloud.gl-inet.cn' ||
    hostName === 'cloud-test.gl-inet.cn' ||
    hostName === 'cloud-dev.gl-inet.cn') {
    return false
  } else if (hostName === 'localhost') {
    return true
  } else {
    return false
  }
}
