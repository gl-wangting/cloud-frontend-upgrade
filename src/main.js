import Vue from 'vue'
import App from './App.vue'
import Router from "vue-router";
Vue.use(Router)

import Routes from '@/router/index.js'
const router = new Router({
  routes: Routes,
  mode: "history"
})
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
