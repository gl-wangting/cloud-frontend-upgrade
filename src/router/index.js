import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);
const routes = [
  {
    path: "/",
    component: () => import("@/views/upgrade/index"),
    redirect: "/upgrade",
  },
  {
    path: "/upgrade",
    name: "upgrade",
    component: () => import("@/views/upgrade/index"),
  },
]
export default routes
